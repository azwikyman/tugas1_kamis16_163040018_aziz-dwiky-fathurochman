

<div class="container mt-5">	
			<div class="card" style="width: 15rem; box-shadow: 0px 5px 20px #AEAEAE ; margin: auto; ">
					<img class="card-img-top" src="<?= BASEURL;?>/img/<?= $data['film']['judul'] ?>.jpg" alt="photo_aziz">
					<div class="card-body">
					    <h5 class="card-title" style="text-align: center;"><?= $data['film']['judul']?></h5>
					    <h6 class="card-subtitle mb-2 text-muted" style="text-align: center;">Release Date : <?= $data['film']['tgl_rilis'] ?></h6>
	 					<p class="card-text" style="text-align: center;"><?= $data['film']['director'] ?></p>
						<p class="card-text" style="text-align: center;"><?= $data['film']['rating'] ?> star</p>
						<p style="text-align: center;">
							<a href="<?= BASEURL ?>/film/" class="card-link">Kembali</a>
						</p>
				  </div>
			</div>
</div>
