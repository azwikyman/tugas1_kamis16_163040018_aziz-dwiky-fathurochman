<div class="container mt-5">
       <div class="container mt-5">
          <h3 style="text-align: center">Edit Film</h3><br>
          <form action="<?= BASEURL; ?>/film/editFilm/$data['film']['id_film']" method="post">

                    <div class="for-group col-4" style="margin: auto">
                         <input type="hidden" name="id_film" value="<?= $data['film']['id_film'] ?>">
                    </div>
                    <div class="form-group col-4" style="margin: auto">
                              <label>Judul:</label>
                              <input type="text" class="form-control" name="judul" value="<?= $data['film']['judul'] ?>">
                    </div><br>

                    <div class="form-group col-4" style="margin: auto">
                              <label>Tanggal Release:</label>
                              <input type="text" class="form-control" name="tgl_rilis" value="<?= $data['film']['tgl_rilis'] ?>">
                    </div><br>
                    <div class="form-group col-4" style="margin: auto">
                              <label>Director:</label>
                              <input type="text" class="form-control" value="<?= $data['film']['director'] ?>" name="director">
                    </div><br>
                    <div class="form-group col-4" style="margin: auto">
                              <label>Rating:</label>
                              <input type="text" class="form-control" value="<?= $data['film']['rating'] ?>" name="rating">
                    </div><br><br>
                    <center>
                         <button type="submit" id="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </center>
          </form>
</div>

</div>
