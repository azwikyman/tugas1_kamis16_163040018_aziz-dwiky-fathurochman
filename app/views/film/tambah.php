<div class="container mt-5">
  <h3 style="text-align: center">Tambah Film</h3><br>
  <form action="<?= BASEURL; ?>/film/insertFilm" method="post">

    <div class="form-group col-4" style="margin: auto">
      <label>Cover:</label>
      <input type="text" class="form-control" placeholder="Masukkan Cover" name="cover">
    </div><br>

    <div class="form-group col-4" style="margin: auto">
      <label>Judul:</label>
      <input type="text" class="form-control" placeholder="Masukkan Judul" name="judul">
    </div><br>

    <div class="form-group col-4" style="margin: auto">
      <label>Tanggal Release:</label>
      <input type="date" class="form-control" placeholder="Masukkan Tanggal" name="tgl_rilis">
    </div><br>

    <div class="form-group col-4" style="margin: auto">
      <label>Director:</label>
      <input type="text" class="form-control" placeholder="Masukkan Director" name="director">
    </div><br>

    <div class="form-group col-4" style="margin: auto">
      <label>Rating:</label>
      <input type="text" class="form-control" placeholder="Masukkan Rating" name="rating">
    </div><br><br>

    <center>
      <button type="submit" id="submit" name="submit" class="btn btn-primary">Tambah</button>
    </center>
  </form>
</div>
