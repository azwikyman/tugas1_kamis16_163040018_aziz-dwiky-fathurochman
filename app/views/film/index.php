<div class="container mt-5">
	
	<div class="row">
		<div class="col-6">
			<h3>Daftar Film</h3>

			<a href="<?= BASEURL; ?>/film/insertFilm" style="text-decoration: none; " class="btn-success btn-sm">Add Film</a >
			<br><br>
			<?php foreach ($data['film'] as $key) : ?>
				<ul class="list-group">
					<li class="list-group-item d-flex justify-content-between align-items-enter">
						<span style="width: 370px;">
							<?php echo $key['judul']; ?>
						</span>

						<a href="<?= BASEURL; ?>/film/detail/<?= $key['id_film'] ?>" class="badge badge-primary" >Detail</a>
						<a href="<?= BASEURL; ?>/film/editFilm/<?=$key['id_film']; ?>" class="badge badge-info">Edit</a>
 			        	<a href="<?= BASEURL; ?>/film/hapusFilm/<?=$key['id_film']; ?>" class="badge badge-danger" onclick = 'return confirm("Apakah anda yakin?")'>Delete</a>
					</li>
				</ul> 
			<?php endforeach; ?>
		</div>
	</div>
</div>

