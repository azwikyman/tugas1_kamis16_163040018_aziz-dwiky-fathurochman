<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?= $data['judul'] ?></title>
		<link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
	</head>
	<body>

			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
				<div class="container">
				  <a class="navbar-brand" href="<?= BASEURL?>">FILM</a>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
				    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				      <li class="nav-item active">
				        <a class="nav-link" href="<?= BASEURL?>">Home <span class="sr-only">(current)</span></a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="<?= BASEURL?>/film">Daftar Film</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link disabled" href="<?= BASEURL?>/about">About</a>
				      </li>
				    </ul>
				    <form class="form-inline my-2 my-lg-0" action="<?= BASEURL;  ?>/film/" method="post">
				      <input class="form-control mr-sm-2" type="text" placeholder="Search" name="search">
				      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="submit">Search
				      </button>
				    </form>
				  </div>
				</nav>
			</div>

		