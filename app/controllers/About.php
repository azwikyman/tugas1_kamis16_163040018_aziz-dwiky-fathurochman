<?php

class About extends  Controller{
	public function page()		
	{
		# code...
		// echo "About/page";
		$data['judul'] = "Pages";
		$this->view('templates/header',$data);
		$this->view('about/page');
		$this->view('templates/footer');

	}

	public function index($nama = "Aziz Dwiky Fathurochman", $job  = "Mahasiswa",$age="20")
	{
		# code...
		// echo "Hello nama saya $nama, saya adalah seorang $job";

		$data = array(
					'nama'=>$nama,
					'job'=>$job,
					'umur'=>$age,
					'judul'=>'About Me'
				);
		$this->view('templates/header',$data);
		$this->view('about/index',$data);
		$this->view('templates/footer');
	}


}