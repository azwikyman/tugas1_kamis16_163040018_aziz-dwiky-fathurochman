<?php 

/**
 * summary
 */
class Film extends Controller
{
    /**
     * summary
     */
    public function index()
    {
        if (isset($_POST["submit"])) {
          $data = array(
            'judul' => "Daftar Film",
            'film' => $this->model('Film_model')->cariFilm($_POST['search'])
          );
          $this->view('templates/header', $data);
          $this->view('film/index', $data);
          $this->view('templates/footer');
        } else {

        $data = array(
        	'judul' => "Daftar Film",
        	'film' => $this->model('Film_model')->getAllFilm()
        );

        $this->view('templates/header',$data);
        $this->view('film/index', $data);
        $this->view('templates/footer');
        }
    }

    public function insertFilm() {
        if (isset($_POST["submit"])) {
          $model = $this->model('Film_model');
          $data['cover'] = $_POST['cover'];
          $data['judul'] = $_POST['judul'];
          $data['tgl_rilis'] = $_POST['tgl_rilis'];
          $data['director'] = $_POST['director'];
          $data['rating'] = $_POST['rating'];

          $model->insertFilm($data);
          header("Location: ../Film");
        } else {
            $model = $this->model('Film_model');
            $data = [
              'judul' => "Tambah Film",
              'film' => $model->getAllFilm()    
            ];
            $this->view('templates/header', $data);
            $this->view('film/tambah', $data);
            $this->view('templates/footer');
        }
      }

    public function editFilm($id) {
        if (isset($_POST["submit"])) {
          $model = $this->model('Film_model');
          $data['id_film'] = $_POST['id_film'];
          $data['cover'] = $_POST['cover'];
          $data['judul'] = $_POST['judul'];
          $data['tgl_rilis'] = $_POST['tgl_rilis'];
          $data['director'] = $_POST['director'];
          $data['rating'] = $_POST['rating'];

          $model->editFilm($data);
          header("Location: ../Film");
        } else {
            $model = $this->model('Film_model');
            $data = [
              'judul' => "Ubah Film",
              'film' => $model->getFilmById($id),
            ];
          $this->view('templates/header', $data);
          $this->view('film/edit', $data);
          $this->view('templates/footer');                  
        }
      }

    public function detail($id){
        $data = array(
            'judul' => 'Daftar Film',
            'film' => $this->model('Film_model')->getFilmById($id)
        );

        $this->view('templates/header',$data);
        $this->view('film/detail',$data);
        $this->view('templates/footer');
    }

    public function hapusFilm($id) {
      $model = $this->model('Film_model');
      $model->hapusFilm($id);
      header("Location: ../Film");
    }

    public function cariFilm($data) {
      if (isset($_POST["submit"])) {
        $model = $this->model('Film_model');
        $data['search'] = $_POST['search'];
        $model->cariFilm($data);             
      } else {
        $model = $this->model('Film_model');
        $data = [
          'judul' => "Cari Film",
          'film' => $model->cariFilm($data)
        ];
        $this->view('templates/header', $data);
        $this->view('film/detail', $data);
        $this->view('templates/footer');
      }
    }
}