<?php 

/**
 * summary
 */
class Film_model
{
    /**
     * summary
     */
        private $table = 'film';
        private $db;

        public function __construct(){
                $this->db = new  Database;
        }

        public function getAllFilm(){
            $this->db->query("SELECT * FROM ".$this->table);
            return $this->db->resultSet();
        }

        public function getFilmById($id){
            $this->db->query("SELECT * FROM ".$this->table. " WHERE id_film=:id");
            $this->db->bind("id",$id);
            return $this->db->single();
        }

        public function insertFilm($data) {
            $cover = $data['cover'];
            $judul  = $data['judul'];
            $tgl_rilis = $data['tgl_rilis'];
            $director = $data['director'];
            $rating = $data['rating'];

            $query = "INSERT INTO $this->table VALUES ('', '$cover', '$judul', '$tgl_rilis', '$director','$rating')";
            $this->db->query($query);
            $this->db->execute();

        }

        public function editFilm($data) {
            $id = $data['id_film'];
            $cover = $data['cover'];
            $judul = $data['judul'];
            $tgl_rilis = $data['tgl_rilis'];
            $director = $data['director'];
            $rating = $data['rating'];

            $query = "UPDATE $this->table SET cover = '$cover', judul = '$judul', tgl_rilis = '$tgl_rilis', director = '$director', rating = $rating WHERE id_film = '$id'";
            $this->db->query($query);
            $this->db->execute();  
        }

        public function hapusFilm($id) {
            $query = "DELETE FROM $this->table WHERE id_film='$id'";
            $this->db->query($query);
            $this->db->execute();
        }

        public function cariFilm($data) {
            $nama = $data;
            $query = "SELECT * FROM " . $this->table . " WHERE judul LIKE '%$nama%'";
            // echo 'test';x
            // var_dump($query);
            $this->db->query($query);
            return $this->db->resultSet();
        }
}