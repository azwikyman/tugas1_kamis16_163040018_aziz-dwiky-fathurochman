/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.21-MariaDB : Database - phpmvc
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`phpmvc` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `phpmvc`;

/*Table structure for table `film` */

DROP TABLE IF EXISTS `film`;

CREATE TABLE `film` (
  `id_film` int(4) NOT NULL AUTO_INCREMENT,
  `cover` varchar(10) DEFAULT NULL,
  `judul` varchar(60) DEFAULT NULL,
  `tgl_rilis` date DEFAULT NULL,
  `director` varchar(30) DEFAULT NULL,
  `rating` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_film`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `film` */

insert  into `film`(`id_film`,`cover`,`judul`,`tgl_rilis`,`director`,`rating`) values (1,'Poster Ven','Venom','2018-10-08','Ruben Fleischer','8.0'),(2,'Poster Ave','Avengers','2017-07-12','Joss Wedhon','8.1'),(3,'Poster Inf','InfinityWar','2018-03-22','Anthony Russo','8.5'),(4,'Poster Jum','Jumanji','2017-11-29','Jake Kasdan','7.2'),(5,'poster mil','Mile22','2018-08-23','Peter Berg','7.0'),(6,'Poster Pel','Pele','2016-12-01','Jeff Zimbalist','7.5'),(7,'poster the','TheFateofTheFurious','2017-07-12','F. Garry Gray','7.3'),(8,'Poster tho','ThorRagnarok','2017-06-22','Taika Waititi','8.2'),(9,'Poster Won','WonderWoman','2017-05-04','Patty Jenkins','8.0'),(10,'Poster Civ','CivilWar','2017-05-25','Anthony Russo','8.5'),(13,'Cover dead','Deadpool','2016-02-12','Tim Miller','8.0');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
